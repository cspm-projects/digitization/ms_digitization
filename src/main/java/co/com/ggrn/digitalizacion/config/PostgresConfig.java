package co.com.ggrn.digitalizacion.config;

import co.com.ggrn.digitalizacion.utils.VarEntornoUtil;
import co.com.ggrn.digitalizacion.utils.secrets.SecretoBDModel;
import co.com.ggrn.digitalizacion.utils.secrets.SecretoConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableAsync
@EnableTransactionManagement
public class PostgresConfig {

    private static final String JDBC_URI = "jdbc:postgresql://%s:%s/%s";
    private static final String DIALECTO_HIBERNATE = "hibernate.dialect";
    private static final String HIBERNATE_DIALECTO_VALOR = "org.hibernate.dialect.PostgreSQLDialect";
    private static final String HIBERNATE_MOSTRAR_SQL = "hibernate.show_sql";
    private static final String HIBERNATE_MOSTRAR_SQL_VALOR = "false";
    private static final String DDL_AUTOMATICO = "hibernate.hbm2ddl.auto";
    private static final String DDL_AUTOMATICO_VALOR = "update";
    private static final String CLASE_DRIVER = "org.postgresql.Driver";
    private static final String CLASE_DRIVER_VALOR = "org.postgresql.Driver";
    private static final String PAQUETE_BASE = "co.com.ggrn.digitalizacion";
    private static final String PAQUETE_ENTIDADES = PAQUETE_BASE + ".models";
    private final SecretoBDModel secretoModel;

    public PostgresConfig() {
        VarEntornoUtil.obtenerInstancia();
        this.secretoModel =  SecretoConfig.obtenerSecreto();
    }

    @Bean
    public DataSource getDataSource() {
        HikariConfig configuracion = new HikariConfig();
        configuracion.setMaximumPoolSize(10);
        configuracion.setJdbcUrl(String.format(JDBC_URI, secretoModel.getHost(),
                secretoModel.getPuerto(), secretoModel.getNombreBD()));
        configuracion.setUsername(secretoModel.getUsuario());
        configuracion.setPassword(secretoModel.getClave());
        return new HikariDataSource(configuracion);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean emFabricaBean =
                new LocalContainerEntityManagerFactoryBean();

        emFabricaBean.setDataSource(dataSource);
        emFabricaBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        emFabricaBean.setPackagesToScan(PAQUETE_ENTIDADES);

        Properties propiedadesJPA = new Properties();

        propiedadesJPA.put(DIALECTO_HIBERNATE, HIBERNATE_DIALECTO_VALOR);
        propiedadesJPA.put(HIBERNATE_MOSTRAR_SQL, HIBERNATE_MOSTRAR_SQL_VALOR);
        propiedadesJPA.put(DDL_AUTOMATICO, DDL_AUTOMATICO_VALOR);
        propiedadesJPA.put(CLASE_DRIVER, CLASE_DRIVER_VALOR);
        emFabricaBean.setJpaProperties(propiedadesJPA);

        return emFabricaBean;
    }

}
