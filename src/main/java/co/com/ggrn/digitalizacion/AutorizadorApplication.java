package co.com.ggrn.digitalizacion;

import co.com.ggrn.digitalizacion.utils.VarEntornoUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutorizadorApplication {
    public static void main(String[] args) {
        SpringApplication.run(AutorizadorApplication.class, args);
        System.out.println(VarEntornoUtil.obtenerValor("SECRET_KEY_CREDIT"));
        System.out.println(VarEntornoUtil.obtenerValor("SECRET_KEY_DEBIT"));
    }
}
