package co.com.ggrn.digitalizacion.utils.secrets;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecretoBDModel {

    private String usuario;
    private String clave;
    private String host;
    private String puerto;
    private String nombreBD;

}
