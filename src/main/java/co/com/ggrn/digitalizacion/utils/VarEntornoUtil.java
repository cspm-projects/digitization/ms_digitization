package co.com.ggrn.digitalizacion.utils;

import io.github.cdimascio.dotenv.Dotenv;

import static co.com.ggrn.digitalizacion.utils.Utils.isProd;

public class VarEntornoUtil {
    private static VarEntornoUtil dotEnvUtil;
    private final Dotenv dotenv;

    private VarEntornoUtil() {
        dotenv = Dotenv.configure()
                .directory("./docker")
                .ignoreIfMalformed()
                .filename("env")
                .ignoreIfMissing()
                .load();
    }

    public static Dotenv obtenerInstancia() {
        if (dotEnvUtil == null) {
            dotEnvUtil = new VarEntornoUtil();
        }
        return dotEnvUtil.obtenerEnv();
    }

    public Dotenv obtenerEnv() {
        return dotenv;
    }

    public static String obtenerValor(String llave) {
        System.out.println(System.getenv("env"));
        System.out.println(System.getenv("enviroment"));
        System.out.println(System.getenv("SECRET_KEY_CREDIT"));
        System.out.println(System.getProperty("env"));
        System.out.println(System.getProperty("enviroment"));
        System.out.println(System.getProperty("SECRET_KEY_CREDIT"));
        if (isProd(System.getenv("env")) ||
                isProd(System.getenv("enviroment"))) {
            return System.getenv(llave);
        }
        return dotEnvUtil.obtenerEnv().get(llave);
    }

}
