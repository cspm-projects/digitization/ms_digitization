package co.com.ggrn.digitalizacion.utils.secrets;

import co.com.ggrn.digitalizacion.utils.VarEntornoUtil;
import com.google.gson.Gson;

public class SecretoConfig {

    private static SecretoBDModel secretoModel;

    public static SecretoBDModel obtenerSecreto() {
        if (secretoModel == null) {
            Gson gson = new Gson();
            secretoModel = gson.fromJson(VarEntornoUtil.obtenerInstancia()
                    .get("CREDENCIALES_BD"), SecretoBDModel.class);
        }
        return secretoModel;
    }

}
